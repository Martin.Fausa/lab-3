package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args){
        String output = "";
        for (String i : args){
            output+= i + " ";
        }
        return output;
    }

    @Override
    public String getName(){
        String output = "echo";
        return output;
    }

}
